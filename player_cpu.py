import random
from debugger import debugger
from terminaltables import AsciiTable #not work in unicode

dd=debugger()
class player_cpu():
    id=None
    name=None
    typer=None
    card_list=[]
    mem=[]
    score=0
    def init_player(self,typer,id):
        #0=player, 1=random cpu, 2=min_max cpu, 3=mem cpu
        self.id=id
        name_list=['player','cpu_r','cpu_n','cpu_x','cpu_m']
        self.name=name_list[typer]+str(id)
        self.typer=typer
        self.card_list=[]
        self.mem=[]
        self.score=0
        
    
    def throw_card(self):
        if self.typer==0:
            return self.player_throw_card()
        elif self.typer==1:
            return self.cpu0_throw_card()
        elif self.typer==2:
            return self.cpu1_throw_card()
        elif self.typer==3:
            return self.cpu2_throw_card()
        
    
    def display_player_card(self):
        table=[]
        header=['slot']
        row=['cards',]
        for i in range(0,13):
            header.append(str(i+1))
            row.append(self.card_list[i])
        print("Your cards:")
        table.append(header)
        table.append(row)
        table=AsciiTable(table)
        print(table.table)
        print('\n')
    
    
    def get_card_detail(self,inputer):
        #dd.d('inputer 51',inputer)
        #dd.d('inputer 52',inputer[0:-1])
        num=int(inputer[0:-1])
        typer=ord(inputer[-1])
        #dd.d('get card detail',(inputer,num,typer))
        return num,typer
    
    def player_throw_card(self):
        answer=input('Choose your card by slot: ')
        if answer.isnumeric() is False:
            print('Choice must be Number, please choose again.')
            return player_throw_card()
        answer=int(answer)
        dd.d('answer',answer)
        answer=answer-1
        if answer<0 or answer>13:
            print('Choice must be bigger than 0 or less than 14, please choose again.')
            return self.player_throw_card()
        if self.card_list[answer]=='':
            print('You have choose a empty slot, please choose again.')
            return self.player_throw_card()
        result=self.card_list[answer]
        self.card_list[answer]=''
        return result
        
    def cpu0_throw_card(self):
        #cpu0 is random player XD
        global player
        ran=random.randint(0,12)
        #dd.d('ran',ran)
        if self.card_list[ran]=='':
            return self.cpu0_throw_card()
        else:
            answer=self.card_list[ran]
            self.card_list[ran]=''
            return answer

    def cpu1_throw_card(self): 
        #cpu1 is small > big
        min_num=14
        min_typer=ord('d')
        min_id=-1
        for i in range(0,12):
            if self.card_list[i]!='':
                #dd.d('player cpu 95 '+str(i),self.card_list)
                num,typer=self.get_card_detail(self.card_list[i])
                #dd.d('player cpu 97',(num,typer))
                if min_num>num or (min_num==num and typer<min_typer):
                    min_id=i
                    min_num=num
                    min_typer=typer
        answer= self.card_list[min_id]           
        self.card_list[min_id]=''
        return answer

    def cpu2_throw_card(self):
        #this cpu choose biggest > smallest:
        min_num=0
        min_typer=ord('d')
        min_id=-1
            
        for i in range(0,12):
            if self.card_list[i]!='':
                num,typer=self.get_card_detail(self.card_list[i])
                if min_num<num or (min_num==num and typer>min_typer):
                    min_id=i
                    min_num=num
                    min_typer=typer
        answer= self.card_list[min_id]           
        self.card_list[min_id]=''
        return answer


    def cpu3_throw_card(self):
        global player
        if len(self.mem)==0 or self.mem==[0,0,0,0,0]:
            #no record, so use smallest card:
            return self.cpu1_throw_card(cpu)
        else:
            max_level=0
            for m in self.mem:
                if max_level==0 or max_level<m:
                    max_level=m
            max_level=max_level+1 #choose the next class as target:
        return None
        
    def save_mem(self,choose):
        global player
        if len(self.mem)==0:
            self.mem=[0,0,0,0,0]
        #dd.d('choose 136',choose)
        for c in choose:
            num,typer=self.get_card_detail(c)
            a=int((num-1)/3)
            #dd.d('a',a)
            self.mem[a]+=1
        #dd.d('mem 140',self.mem)
        
    def display_cpu3_men(self):
        global player
        if len(self.mem)>0:
            table=[]
            header=['classes']
            row=['count',]
            class_list=['1-3','4-6','7-9','10-12','13']
            for c in range(0,5):
                header.append(class_list[c])
                row.append(self.mem[c])
            table.append(header)
            table.append(row)
            print('cpu mem:')
            table=AsciiTable(table)
            print(table.table)
            print('\n')
