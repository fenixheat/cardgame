'''
date:2011271039
usage:
from debugger import debugger
debugger.xxx()
'''

import argparse
import sys
import os
import csv
import cv2
from collections import namedtuple
#from bunch import bunchify
class debugger:
    #items=[{'name':'','helper':'','default_value':''},]
    def get_sys_argv(self, items):
        parser = argparse.ArgumentParser()
        for item in items:
            if(item['default_value']==''):
                parser.add_argument('--'+item['name'],help=item['helper'],required=True)
            else:
                parser.add_argument('--'+item['name'],help=item['helper'],default=item['default_value'])
        args=parser.parse_args()
        print(args)
        return args

    def d(self,label,content=''):
        if(label!=''):
            print('=========== {} ============'.format(label))
        else: print('===========================')
        if(content!=''):
            print(content)

    def dd(self,label,content):
        print('=========== {} ============'.format(label))
        print('{}'.format(content))

    def get_basename(self,filename):
        return os.path.splitext(filename)

    def mkdir(self, folder_name):
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)
            return folder_name
        if(os.path.isdir(folder_name)):
            return folder_name
        else:
            os.mkdir(folder_name)
            return folder_name

    def image_to_jpg(self,target,destination):
        image=cv2.imread(target)
        cv2.imwrite(destination, image, [int(cv2.IMWRITE_JPEG_QUALITY), 100])

    def obj_2_dic(self, d):
        top = type('new', (object,), d)
        seqs = tuple, list, set, frozenset
        for i, j in d.items():
            if isinstance(j, dict):
                setattr(top, i, obj_dic(j))
            elif isinstance(j, seqs):
                setattr(top, i,
                        type(j)(obj_dic(sj) if isinstance(sj, dict) else sj for sj in j))
            else:
                setattr(top, i, j)
        return top


    def show_img(self,filename,img,coor):
      if len(img.shape)>2:
          height,width,depth=img.shape
      else:
          height,width=img.shape
      display=self.draw_image(img,coor)
      #dd.d('40','{}:{}'.format(width,height))
      if height<640 or width<640 or width>960 or height>960:
          display=cv2.resize(display,(640,640),interpolation = cv2.INTER_AREA)
      cv2.imshow(filename,display)
      cv2.waitKey(0)
      cv2.destroyAllWindows()

    def draw_img(self, img,coor,thick=5):
      display=img.copy()
      if not coor is None:
         dd.dd('32',coor)
         for data in coor:
             display=cv2.rectangle(display,(data['box'][0],data['box'][1]),(data['box'][2],data['box'][3]),(255,0,0),thick)
             display = cv2.circle(display, (data['mid_point'][0], data['mid_point'][1]), thick, (0, 0, 255), thick)
      return display

    def array_to_csv(self,arrays,filename):
        if(len(arrays)>0):
            keys = arrays[0].keys()
            with open(filename, 'w', newline='')  as output_file:
                dict_writer = csv.DictWriter(output_file, keys)
                dict_writer.writeheader()
                dict_writer.writerows(arrays)
                return filename
        else: return filename

    def csv_to_json(self,filename):
        fh = csv.reader(open(filename, "rU"), delimiter=',', dialect=csv.excel_tab)
        #headers = fh.next()
        headers=next(fh)
        Row = namedtuple('Row', headers)
        list_of_dicts = [Row._make(i)._asdict() for i in fh]
        return list_of_dicts
