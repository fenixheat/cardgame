import random
from terminaltables import AsciiTable #not work in unicode
#from beautifultable import BeautifulTable
from debugger import debugger
from player_cpu import player_cpu
dd=debugger()
card=[]
player=[]

test_player=[3,2,1,1] #3=x,2=n,1=r,

def init_player(player_type):
        global player
        #0=player, 1=random cpu, 2=min_max cpu, 3=mem cpu
        player=[]
        for i in range(0,4):
            player.append(player_cpu())
            player[i].init_player(player_type[i],i)
        
def ask_game_mode():
    answer=input('choose 1=player, 2=auto_test: ')
    if not int(answer) in [1,2]:
        print('wrong input. Please input again.')
        return ask_game_mode()
    else:
        return answer
        
def apply_one_card(i):
            global player,card
            success=False
            cc=random.randint(0,51)
            #dd.d('cc 17',cc)
            if card[cc]!='':
                #dd.d('player list 27',i)
                if len(player[i].card_list)<13:
                    player[i].card_list.append(card[cc])
                    card[cc]=''
                    success=True
                else:
                    success=True #cannot apply card...
                
            #dd.d('card 21',card)
            #dd.d('player 20 '+str(i),player[i].card_list)
            if success is False:
                return apply_one_card(i)

def display_game_rules():
    texter='-. Full cards, means 52 cards no Joker.\n'
    texter+='-. 4 Players, each player get 13 cards.\n'
    texter+='-. In one round, everyone throw one cards. Biggest cards player can get all cards with scores\n'
    texter+='-. Card score is its number.\n'
    texter+='-. Max scores to win.\n'
    dd.d('Game Rules',texter)
    problemer='-. Cannot use unicode to display the card type. It will break the display table...\n'
    problemer+='   temper fixed: a='+u'\u2664'+', b='+u'\u2665'+', c='+u'\u2667'+', d='+u'\u2666'+'\n'
    problemer+='   maybe I should write it on html+js...'
    dd.d('Known problem',problemer)
    
            
def display_player():
    global player
    table=[]
    header=['id']
    score=['score']
    for i in range(0,4):
        name=player[i].name
        header.append(name)
        score.append(player[i].score)
    table.append(header)
    table.append(score)
    table=AsciiTable(table)
    print("Score board:")
    print(table.table)
    print('\n')


def check_card_and_apply_score(choose):
    global player
    max_num=0
    max_type=0
    max_id=0
    score=0
    #dd.d('c 113',choose)
    for c in range(0,4):
        num,typer=player[0].get_card_detail(choose[c])
        score+=num
        if max_id==0 and c==0:
            max_num=num
            max_type=typer
        else:
            if max_num<num or (max_num==num and max_type>typer): #a>c
                max_num=num
                max_type=typer
                max_id=c
    player[max_id].score=player[max_id].score+score
    display_card_result(choose,score,max_id)
    return max_id,score
                
def display_card_result(choose,score,max_id):
    global player
    table=[]
    header=['player']
    row=['cards',]
    for i in range(0,4):
        header.append(player[i].name)
        row.append(choose[i])
    header.append("Total score")
    row.append(score)
    print("Choose cards result:")
    table.append(header)
    table.append(row)
    table=AsciiTable(table)
    print(table.table)
    print(player[max_id].name+" win! He get "+str(score)+' scores')
    print('\n')
    
def find_win_player():
    global player
    max_id=0
    max_score=0
    for i in range(0,4):
        if player[i].score>max_score:
            max_score=player[i].score
            max_id=i
    return max_id

def save_game_result(max_id,score,choose):
        global player
        temp={
            player[0].name:choose[0],
            player[1].name:choose[1],
            player[2].name:choose[2],
            player[3].name:choose[3],
            'score':score,
            'win_player':player[max_id].name
        }
        return temp


def game_loop(game_mode):
    global player,card
    dd.d('game start','\n')
    #dd.d('card',card)
    is_end=0
    game_result=[]        
    while is_end<=12:
        if game_mode=='1':
            display_player()
            player[0].display_cpu3_men()
            player[0].display_player_card()
        choose=[]
        for i in range(0,4):
            choose.append(player[i].throw_card())
        #dd.d('choose',choose) #work
        player[0].save_mem(choose)
        max_id,score=check_card_and_apply_score(choose)
        temp=save_game_result(max_id,score,choose)
        game_result.append(temp)
        is_end=is_end+1
    print('game is end')
    display_player()
    max_id=find_win_player()
    print(player[max_id].name+' win the game.')
    if game_mode=='1':
        ddd=datetime.today().strftime('%Y%m%d%H%i%s')
        dd.array_to_csv(game_result,'game_'+ddd+'.csv')   
    return max_id,score
        
    
def init_random_card():
    global player,card
    #init card list:
    card=[]
    for j in range(1,14):
        #for i in [u'\u2664',u'\u2665',u'\u2667',u'\u2666']: #not work in printing table....
        for i in ['a','b','c','d']:
            card.append(str(j)+i)
    #dd.d('card',card) #work
    #init player card:
    for i in range(0,4):
        for j in range(0,13):
            apply_one_card(i)
            
def main():
    global player,card
    game_mode=ask_game_mode()
    if game_mode=='1':    
        init_player([0,1,2,3])    
        init_random_card()
        display_game_rules()
        game_loop(game_mode)   
    else:
        init_player(test_player)    
        test_result={
            player[0].name:0,
            player[1].name:0,
            player[2].name:0,
            player[3].name:0,
        }
        dd.d('auto test begin, player list',test_player)
        for i in range(0,5000):
            print('============ round '+str(i)+' ===========')
            init_player(test_player)    
            init_random_card()
            max_id,score=game_loop(game_mode)  
            test_result[player[max_id].name]+=1
        print('test end')
        print('test result',test_result)        

main()